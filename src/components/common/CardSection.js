import React, { PropTypes } from 'react';
import { View } from 'react-native';

const CardSection = (props) => {
  return (
    <View style={styles.containerStyle}>
      {props.children}
    </View>
  );
};

CardSection.propTypes = {
  children: PropTypes.arrayOf(PropTypes.element).isRequired
};

const styles = {
  containerStyle: {
    borderBottomWidth: 1,
    padding: 5,
    backgroundColor: '#FFFFFF',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    borderColor: '#DDDDDD',
    position: 'relative',
  }
};

export { CardSection };
