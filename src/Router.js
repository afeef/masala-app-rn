import React from 'react';
import { Scene, Router } from 'react-native-router-flux';
import LoginForm from './components/LoginForm';
import EmployeeList from './components/EmployeeList';

const RouterComponent = () => {
  return (
    <Router sceneStyle={styles}>
      <Scene key="login" component={LoginForm} title="Please Login" initial />
      <Scene key="employeeList" component={EmployeeList} title="Employees" />
    </Router>
  );
};

const styles = {
  paddingTop: 60
};

export default RouterComponent;
